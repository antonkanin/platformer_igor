﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Xml;
using System.Reflection;
using System;

public class SpriteXmlSlicerWindow : EditorWindow
{
    private Texture2D m_texture;
    private TextAsset m_xmlAsset;

    [MenuItem("Window/SpriteSlicer/Xml")]
    public static void ShowWindow()
    {
        GetWindow(typeof(SpriteXmlSlicerWindow));
    }

    private void OnGUI()
    {
        m_texture = EditorGUILayout.ObjectField("Sprite: ", m_texture, typeof(Texture2D), false, GUILayout.MaxWidth(350)) as Texture2D;
        m_xmlAsset = EditorGUILayout.ObjectField("Xml: ", m_xmlAsset, typeof(TextAsset), false, GUILayout.MaxWidth(350)) as TextAsset;


        if (GUILayout.Button("Slice", GUILayout.MaxWidth(350)))
        {
            if (m_texture == null)
            {
                Debug.Log("Please select a texture");
                return;
            }

            if (m_xmlAsset == null)
            {
                Debug.Log("Please select a Xml file");
                return;
            }

            var path = AssetDatabase.GetAssetPath(m_texture);
            var ti = AssetImporter.GetAtPath(path) as TextureImporter;
            ti.isReadable = true;

            ClearTextureImporter(path, ti);
            SliceSprites(path, ti, m_texture, m_xmlAsset);
        }
    }

    private static void ClearTextureImporter(string path, TextureImporter ti)
    {
        ti.spriteImportMode = SpriteImportMode.Single;
        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
    }

    private static void SliceSprites(string path, TextureImporter ti, Texture2D tex, TextAsset xmlAsset)
    {
        var xmlPath = AssetDatabase.GetAssetPath(xmlAsset);
        var xmlDoc = new XmlDocument();
        xmlDoc.Load(xmlPath);

        //Create TextureAtlasContainer from XML file by parsing it
        var container = new TextureAtlasContainer();
        foreach (XmlNode node in xmlDoc.GetElementsByTagName("SubTexture"))
        {
            var subTexture = new SubTexture();
            subTexture.AddAttributes(node.Attributes);
            container.SubTextures.Add(subTexture);
        }

        //Write all data from TextureAtlasContainer to TextureImporter
        ti.isReadable = true;
        ti.spriteImportMode = SpriteImportMode.Multiple;
        var newData = new List<SpriteMetaData>();

        foreach (var subText in container.SubTextures)
        {
            var smd = new SpriteMetaData();
            var x = (subText.Width * 0.5f + subText.FrameX) / subText.Width;
            var y = 1 - (subText.Height * 0.5f + subText.FrameY) / subText.Height;

            //Pivot is normalized value
            //In Unity Pivot (0, 0) is bottom-left corner
            smd.pivot = new Vector2(x, y);

            //Center = 0, TopLeft = 1, TopCenter = 2, TopRight = 3, LeftCenter = 4, RightCenter = 5,
            //BottomLeft = 6, BottomCenter = 7, BottomRight = 8, Custom = 9
            //Explanation: https://docs.unity3d.com/ScriptReference/SpriteMetaData-alignment.html
            smd.alignment = 9;
            smd.name = subText.Name;
            smd.rect = new Rect(subText.X, tex.height - subText.Y - subText.Height, subText.Width,
                subText.Height);
            newData.Add(smd);
        }

        //Store data on disk
        ti.spritesheet = newData.ToArray();
        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
    }

    private class TextureAtlasContainer
    {
        public List<SubTexture> SubTextures = new List<SubTexture>();
    }

    private class SubTexture
    {
        public string Name = "";
        public int FrameHeight = 0;
        public int FrameWidth = 0;
        public int FrameX = 0;
        public int FrameY = 0;
        public int Height = 0;
        public int Width = 0;
        public int X = 0;
        public int Y = 0;

        public void AddAttributes(XmlAttributeCollection attributes)
        {
            foreach (XmlAttribute attribute in attributes) AddAttribute(attribute);
        }

        public void AddAttribute(XmlAttribute attribute)
        {
            AddAttribute(attribute.Name, attribute.Value);
        }

        public void AddAttribute(string atrName, string atrValue)
        {
            var fields = GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var field in fields)
            {
                if (string.Equals(atrName, "name", StringComparison.CurrentCultureIgnoreCase))
                {
                    Name = atrValue;
                    continue;
                }

                if (!string.Equals(field.Name, atrName, StringComparison.CurrentCultureIgnoreCase))
                {
                    continue;
                }

                var val = 0;
                int.TryParse(atrValue, out val);

                field.SetValue(this, val);
            }
        }
    }
}

