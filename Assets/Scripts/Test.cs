﻿using UnityEngine;
using DG.Tweening;

public class Test : MonoBehaviour
{
	public Vector3 point1;
	public Vector3 point2;
	public float duration = 1f;
	public Ease Ease;
	public AnimationCurve curve;

	private void Start()
	{
		var anim1 = transform.DOMove(point1, duration).SetEase(curve);
		var anim2 = transform.DOMove(point2, duration).SetEase(curve);

		var sequence = DOTween.Sequence();
		sequence.Append(anim1);
		sequence.AppendCallback(Print);
		sequence.AppendInterval(2f);
		sequence.Append(anim2);
		sequence.Play();
	}

	private void Print()
	{
		Debug.Log("Hi!");
	}
}
