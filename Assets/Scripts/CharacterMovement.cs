﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterMovement : MonoBehaviour
{
	[SerializeField] private float m_speed = 5f;
	private Rigidbody2D m_rb2d;

	private void Start()
	{
		m_rb2d = GetComponent<Rigidbody2D>();
	}

	private void FixedUpdate()
	{
		m_rb2d.velocity = Vector2.right * Input.GetAxis("Horizontal") * m_speed;
	}
}
