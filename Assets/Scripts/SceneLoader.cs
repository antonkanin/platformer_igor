﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
	[SerializeField] private float m_delay;

	private const string LOADING_SCENE = "Loading";
	private static string m_nextScene;
	private static bool m_isAsync;

	public static void LoadScene(string sceneName, bool async = true)
	{
		m_nextScene = sceneName;
		m_isAsync = async;
		SceneManager.LoadScene(LOADING_SCENE);
	}

	private IEnumerator Start()
	{
		yield return new WaitForSeconds(m_delay);

		if (string.IsNullOrEmpty(m_nextScene))
		{
			Debug.LogError("Invalid scene");
			yield break;
		}

		if (!m_isAsync)
		{
			SceneManager.LoadScene(m_nextScene);
			yield break;
		}

		var loading = SceneManager.LoadSceneAsync(m_nextScene, LoadSceneMode.Additive);

		while (!loading.isDone)
		{
			yield return null;
		}

		SceneManager.UnloadSceneAsync(LOADING_SCENE);
	}
}
