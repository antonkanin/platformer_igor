﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SOArchitecture/RuntimeSet", fileName = "NewRuntimeSet")]
public class RuntimeSet : ScriptableObject, IEnumerable<GameObject>
{
	private List<GameObject> m_set = new List<GameObject>();

	private void OnEnable()
	{
		m_set.Clear();
	}

	public void Register(GameObject item)
	{
		if (!m_set.Contains(item))
		{
			m_set.Add(item);
		}
	}

	public void UnRegister(GameObject item)
	{
		if (m_set.Contains(item))
		{
			m_set.Remove(item);
		}
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		yield return GetEnumerator();
	}

	public IEnumerator<GameObject> GetEnumerator()
	{
		foreach (var item in m_set)
		{
			yield return item;
		}
	}
}
