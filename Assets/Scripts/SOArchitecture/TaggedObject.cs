﻿using UnityEngine;

public class TaggedObject : MonoBehaviour
{
	[SerializeField] private RuntimeSet[] m_tags;

	private void OnEnable()
	{
		foreach (var tag in m_tags)
		{
			tag.Register(this.gameObject);
		}
	}

	private void OnDisable()
	{
		foreach (var tag in m_tags)
		{
			tag.UnRegister(gameObject);
		}
	}
}
