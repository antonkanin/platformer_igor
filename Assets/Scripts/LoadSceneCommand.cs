﻿using UnityEngine;

[CreateAssetMenu]
public class LoadSceneCommand : ScriptableObject
{
	public void LoadScene(string sceneName)
	{
		SceneLoader.LoadScene(sceneName);
	}
}
